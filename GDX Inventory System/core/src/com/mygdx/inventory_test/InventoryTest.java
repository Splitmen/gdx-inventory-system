package com.mygdx.inventory_test;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL30;
import com.mygdx.inventory_test.entity.Entity;
import com.mygdx.inventory_test.entity.EntityHuman;
import com.mygdx.inventory_test.inventory_system.item.Item;
import com.mygdx.inventory_test.inventory_system.item.debug.TestItem;

public class InventoryTest extends Game {
	
	public static final String DEBUG_TITLE = "Main Class";
	public Entity[] entitys;
	private Item testItem;
	
	@Override
	public void create () {
		Gdx.app.log(DEBUG_TITLE, "create()");
		
		entitys = new Entity[] {new EntityHuman()};
		
		testItem = new TestItem(9999, 1);
		testItem.getIcon().setX(100);
		testItem.getIcon().setY(100);
		((EntityHuman)entitys[0]).getInvs()[0].add(testItem);
		
//		Gdx.input.setInputProcessor(Gdx.input.getInputProcessor());
	}
	
	public void update() {
//		Gdx.app.log(DEBUG_TITLE, "update()");
		
		entitys[0].update(Gdx.graphics.getDeltaTime());
	}

	@Override
	public void render () {
		update();
		super.render();
//		Gdx.app.log(DEBUG_TITLE, "render()");
		Gdx.gl.glClearColor(0, 1, 0, 1);
		Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);
		
		entitys[0].render(Gdx.graphics.getDeltaTime());
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		Gdx.app.log(DEBUG_TITLE, "resize("+width+", "+height+")");
		
		entitys[0].resize(width, height);
	}

	@Override
	public void pause() {
		super.pause();
		Gdx.app.log(DEBUG_TITLE, "pause()");
		
		entitys[0].pause();;
	}

	@Override
	public void resume() {
		super.resume();
		Gdx.app.log(DEBUG_TITLE, "resume()");
		
		entitys[0].resume();;
	}
	
	@Override
	public void dispose () {
		super.dispose();
		Gdx.app.log(DEBUG_TITLE, "dispose()");
		
		entitys[0].dispose();;
	}
}

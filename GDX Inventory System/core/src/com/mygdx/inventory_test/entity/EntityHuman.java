package com.mygdx.inventory_test.entity;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.mygdx.inventory_test.gui.GUI;
import com.mygdx.inventory_test.gui.HumanoidInventoryGUI;
import com.mygdx.inventory_test.inventory_system.FixedInventory;

public class EntityHuman extends Entity {
	
	private String name;
		
	private FixedInventory[] invs;
	
	private GUI invGUI;

	public EntityHuman() {
		init();
	}
	
	private void init() {
		invs = new FixedInventory[] {new FixedInventory("item_inv", 30), new FixedInventory("equip_inv", 8)};
		
		invGUI = new HumanoidInventoryGUI(0);
		
		Gdx.input.setInputProcessor(Gdx.input.getInputProcessor());
	}

	public void update(float delta) {
		// check for key press and open/close the inventory
		if(Gdx.input.isKeyJustPressed(Keys.I) && invGUI.isOpen() == false) {
			invGUI.setInvOpen(true);
			invGUI.show();
			System.out.println("i pressed inv opened");
		} else if(Gdx.input.isKeyJustPressed(Keys.I) && invGUI.isOpen() == true) {
			invGUI.setInvOpen(false);
			invGUI.hide();
			System.out.println("i pressed inv closed");
		}
		// update the inventory if necessary.
		if(invGUI.isOpen() == true) invGUI.update(delta);
	}

	public void render(float delta) {
		// render the inventory if necessary.
		if(invGUI.isOpen() == true) invGUI.render(delta);
	}

	public void resize(int width, int height) {
		// resize the inventory
		invGUI.resize(width, height);
	}

	public void pause() {
		// pause the inventory if necessary.
		if(invGUI.isOpen() == true) invGUI.pause();
	}

	public void resume() {
		// resume the inventory if necessary.
		if(invGUI.isOpen() == true) invGUI.resume();
	}

	public void dispose() {
		// dispose the inventory
		invGUI.dispose();
	}

	/*
	 * Getter and setter.
	 */
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public FixedInventory[] getInvs() {
		return invs;
	}

	public void setInvs(FixedInventory[] invs) {
		this.invs = invs;
	}

}

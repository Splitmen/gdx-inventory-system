package com.mygdx.inventory_test.entity;

public abstract class Entity {
	
	public abstract void update(float delta);

	public abstract void render(float delta);

	public abstract void resize(int width, int height);

	public abstract void pause();

	public abstract void resume();

	public abstract void dispose();
	
	/*
	 * Getters and setters.
	 */
}

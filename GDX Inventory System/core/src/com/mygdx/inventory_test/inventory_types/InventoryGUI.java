package com.mygdx.inventory_test.inventory_types;

import com.badlogic.gdx.Screen;
import com.mygdx.inventory_test.inventory_list_system.FixedInventoryList;

public abstract class InventoryGUI implements Screen {
	
	private FixedInventoryList[] invs;
	private boolean isOpen;
	
	public abstract void update(float delta);
	
	/*
	 * Getter and setter.
	 */
	public FixedInventoryList[] getInvs() {
		return invs;
	}

	public void setInvs(FixedInventoryList[] invs) {
		this.invs = invs;
	}

	public boolean isOpen() {
		return isOpen;
	}

	public void setInvOpen(boolean isOpen) {
		this.isOpen = isOpen;
	}
}

package com.mygdx.inventory_test.inventory_types;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.mygdx.inventory_test.inventory_list_system.FixedInventoryList;

public class HumanoidInventoryScreen extends InventoryGUI {
	
	public static final String DEBUG_TITLE = "Humanoid Fixed Inventory";
	
	private FixedInventoryList itemInv;
	private FixedInventoryList equipInv;
	
	private Stage stage;
	private TextureAtlas atlas;
	private Skin skin;
	
	public HumanoidInventoryScreen(String ID, int invSize) {
		setItemInv(new FixedInventoryList(ID, invSize));
		setEquipInv(new FixedInventoryList(ID, 8));
		setInvs(new FixedInventoryList[] {
				getItemInv(), getEquipInv()
		});
	}
	
	public HumanoidInventoryScreen(String ID, String name, int invSize) {
		setItemInv(new FixedInventoryList(ID, name, invSize));
		setEquipInv(new FixedInventoryList(ID, name, 8));
		setInvs(new FixedInventoryList[] {
				getItemInv(), getEquipInv()
		});
	}

	@Override
	public void show() {
		Gdx.app.log(DEBUG_TITLE, "show()");
		
//		atlas = new TextureAtlas("slot.pack");
//		skin = new Skin(atlas);
		
		new BitmapFont(Gdx.files.internal("Franklin Gothic Medium 32pix.fnt"), false);
	}
	
	public void update(float delta) {
//		Gdx.app.log(DEBUG_TITLE, "update("+delta+")");
	}

	@Override
	public void render(float delta) {
		update(delta);
//		Gdx.app.log(DEBUG_TITLE, "render("+delta+")");
		Gdx.gl.glClearColor(0, 1, 0, 1);
		Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);
	}

	@Override
	public void resize(int width, int height) {
		Gdx.app.log(DEBUG_TITLE, "resize("+width+", "+height+")");
	}

	@Override
	public void pause() {
		Gdx.app.log(DEBUG_TITLE, "pause()");
	}

	@Override
	public void resume() {
		Gdx.app.log(DEBUG_TITLE, "resume()");
	}

	@Override
	public void hide() {
		Gdx.app.log(DEBUG_TITLE, "hide()");
	}

	@Override
	public void dispose() {
		Gdx.app.log(DEBUG_TITLE, "dispose()");
	}
	
	/*
	 * Getters and setters.
	 */
	public FixedInventoryList getItemInv() {
		return itemInv;
	}

	public void setItemInv(FixedInventoryList itemInv) {
		this.itemInv = itemInv;
	}

	public FixedInventoryList getEquipInv() {
		return equipInv;
	}

	public void setEquipInv(FixedInventoryList equipInv) {
		this.equipInv = equipInv;
	}

}

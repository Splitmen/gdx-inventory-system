package com.mygdx.inventory_test.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.mygdx.inventory_test.InventoryTest;
import com.mygdx.inventory_test.entity.EntityHuman;
import com.mygdx.inventory_test.inventory_system.FixedInventory;

public class HumanoidInventoryGUI extends GUI {
	
	private int entityIndex;
	private FixedInventory[] invs;
	
	private Table table;
	
	public static final String DEBUG_TITLE = "Humanoid Fixed Inventory";
	
	public HumanoidInventoryGUI(int entityIndex) {
		setEntityIndex(entityIndex);
	}

	@Override
	public void show() {
		super.show();
		Gdx.app.log(DEBUG_TITLE, "show()");
		
		invs = ((EntityHuman)((InventoryTest)Gdx.app.getApplicationListener()).entitys[getEntityIndex()]).getInvs();
		
		fGM32 = new BitmapFont(Gdx.files.internal("Franklin Gothic Medium 32pix.fnt"), false);
		
		stage = new Stage();
		atlas = new TextureAtlas(Gdx.files.internal("gui/slot.pack"));
		skin = new Skin(atlas);
		table = new Table(skin);
	}
	
	public void update(float delta) {
//		Gdx.app.log(DEBUG_TITLE, "update("+delta+")");
	}

	@Override
	public void render(float delta) {
		update(delta);
		Gdx.app.log(DEBUG_TITLE, "render("+delta+")");
		Gdx.gl.glClearColor(0, 1, 0, 1);
		Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);
	}

	@Override
	public void resize(int width, int height) {
		Gdx.app.log(DEBUG_TITLE, "resize("+width+", "+height+")");
	}

	@Override
	public void pause() {
		Gdx.app.log(DEBUG_TITLE, "pause()");
	}

	@Override
	public void resume() {
		Gdx.app.log(DEBUG_TITLE, "resume()");
	}

	@Override
	public void hide() {
		Gdx.app.log(DEBUG_TITLE, "hide()");
	}

	@Override
	public void dispose() {
		Gdx.app.log(DEBUG_TITLE, "dispose()");
	}
	
	/*
	 * Getters and setters.
	 */
	public int getEntityIndex() {
		return entityIndex;
	}

	public void setEntityIndex(int entityIndex) {
		this.entityIndex = entityIndex;
	}

	public FixedInventory[] getInvs() {
		return invs;
	}

	public void setInvs(FixedInventory[] invs) {
		this.invs = invs;
	}
}

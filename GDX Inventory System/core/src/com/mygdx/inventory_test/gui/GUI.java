package com.mygdx.inventory_test.gui;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public abstract class GUI {

	private boolean isOpen;
	
	protected Stage stage;
	protected TextureAtlas atlas;
	protected Skin skin;
	protected BitmapFont fGM32;
	
	public void show() {
		
	}
	
	public abstract void update(float delta);
	
	public abstract void render(float delta);

	public abstract void resize(int width, int height);

	public abstract void pause();

	public abstract void resume();

	public abstract void hide();

	public abstract void dispose();
	
	/*
	 * Getter and setter.
	 */
	public boolean isOpen() {
		return isOpen;
	}

	public void setInvOpen(boolean isOpen) {
		this.isOpen = isOpen;
	}
}

package com.mygdx.inventory_test.inventory_system.item.material.ingot;

import com.mygdx.inventory_test.inventory_system.item.Item;

public abstract class Ingot extends Item {
	
	// Item types
	public static final String ITEMTYPE_INGOT = "Ingot";

	public Ingot(String ID, String name, String description, int grade, int stackSize) {
		super(ID, name, description, grade, -1, -1, 9999, stackSize);
		addItemType(ITEMTYPE_MATERIAL);
		addItemType(ITEMTYPE_INGOT);
	}

}

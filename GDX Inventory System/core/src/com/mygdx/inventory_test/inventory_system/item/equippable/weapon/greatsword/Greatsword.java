package com.mygdx.inventory_test.inventory_system.item.equippable.weapon.greatsword;

import com.mygdx.inventory_test.inventory_system.item.equippable.weapon.Weapon;

public abstract class Greatsword extends Weapon {
	
	// Item types
	public static final String ITEMTYPE_GREATSWORD = "Greatsword";
	
	/**
	 * Placeholder constructor.
	 */
	public Greatsword(String ID, String name, String description, int grade, int maxDurability, int durebility, float physicalDmg, float magicDmg, float critDmg) {
		super(ID, name, description, grade, maxDurability, durebility, physicalDmg, magicDmg, 1.9f, critDmg, 1.6f, 0.15f);
		addItemType(ITEMTYPE_GREATSWORD);
	}

}

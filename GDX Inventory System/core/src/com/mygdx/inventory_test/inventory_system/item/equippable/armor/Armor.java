package com.mygdx.inventory_test.inventory_system.item.equippable.armor;

import com.mygdx.inventory_test.inventory_system.item.equippable.Equippable;

public abstract class Armor extends Equippable {
	
	// Item types
	public static final String ITEMTYPE_Armor = "Armor";
	
	/**
	 * Placeholder constructor.
	 */
	public Armor(String ID, String name, String description, int grade, int maxDurability, int durebility, float physicalDef, float magicDef, float defM) {
		super(ID, name, description, grade, maxDurability, durebility, physicalDef, magicDef, defM);
		addItemType(ITEMTYPE_Armor);
	}

}

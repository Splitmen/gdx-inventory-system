package com.mygdx.inventory_test.inventory_system.item.equippable.armor.feet;

import com.mygdx.inventory_test.inventory_system.item.equippable.armor.Armor;

public abstract class Feet extends Armor {
	
	// Item types
	public static final String ITEMTYPE_FEET = "Feet";
	
	/**
	 * Placeholder constructor.
	 */
	public Feet(String ID, String name, String description, int grade, int maxDurability, int durebility, float physicalDef, float magicDef, float defM) {
		super(ID, name, description, grade, maxDurability, durebility, physicalDef, magicDef, defM);
		addItemType(ITEMTYPE_FEET);
	}

}

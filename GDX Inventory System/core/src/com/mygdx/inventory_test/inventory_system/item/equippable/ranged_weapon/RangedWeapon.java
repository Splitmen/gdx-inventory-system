package com.mygdx.inventory_test.inventory_system.item.equippable.ranged_weapon;

import com.mygdx.inventory_test.inventory_system.item.equippable.weapon.Weapon;

public abstract class RangedWeapon extends Weapon {
	
	// Item types
	public static final String ITEMTYPE_RANGED_WEAPON = "Ranged Weapon";
	
	// Ranged weapon variables
	private float rSpeed;
	private float range;
	
	/**
	 * Placeholder constructor.
	 */
	public RangedWeapon(String ID, String name, String description, int grade, int maxDurability, int durebility, float physicalDmg, float magicDmg, float dmgM, float critDmg, float critDmgM, float critChance, float rSpeed, float range) {
		super(ID, name, description, grade, maxDurability, durebility, physicalDmg, magicDmg, dmgM, critDmg, critDmgM, critChance);
		addItemType(ITEMTYPE_RANGED_WEAPON);
		setRSpeed(rSpeed);
		setRange(range);
	}
	
	/*
	 * Getters and setters.
	 */
	public float getRSpeed() {
		return rSpeed;
	}

	public void setRSpeed(float rSpeed) {
		this.rSpeed = rSpeed;
	}

	public float getRange() {
		return range;
	}

	public void setRange(float range) {
		this.range = range;
	}

}

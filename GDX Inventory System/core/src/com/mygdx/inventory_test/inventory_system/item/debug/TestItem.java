package com.mygdx.inventory_test.inventory_system.item.debug;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.mygdx.inventory_test.inventory_system.item.Item;

public class TestItem extends Item {

	public TestItem(int maxStackSize) {
		super(new Image(new Texture(Gdx.files.internal("badlogic.jpg"))), "test_item", "Test Item", maxStackSize, maxStackSize);
		addItemType(ITEMTYPE_DEBUG);
	}

	public TestItem(int maxStackSize, int stackSize) {
		super(new Image(new Texture(Gdx.files.internal("badlogic.jpg"))), "test_item", "Test Item", maxStackSize, stackSize);
		addItemType(ITEMTYPE_DEBUG);
	}

}

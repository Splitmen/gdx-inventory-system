package com.mygdx.inventory_test.inventory_system.item.equippable.weapon.wakizashi;

import com.mygdx.inventory_test.inventory_system.item.equippable.weapon.Weapon;

public abstract class Wakizashi extends Weapon {
	
	// Item types
	public static final String ITEMTYPE_WAKIZASHI = "Wakizashi";
	
	/**
	 * Placeholder constructor.
	 */
	public Wakizashi(String ID, String name, String description, int grade, int maxDurability, int durebility, float physicalDmg, float magicDmg, float dmgM, float critDmg, float critDmgM, float critChance) {
		super(ID, name, description, grade, maxDurability, durebility, physicalDmg, magicDmg, dmgM, critDmg, critDmgM, critChance);
		addItemType(ITEMTYPE_WAKIZASHI);
	}

}

package com.mygdx.inventory_test.inventory_system.item.equippable.armor.legs;

import com.mygdx.inventory_test.inventory_system.item.equippable.armor.Armor;

public abstract class Legs extends Armor {
	
	// Item types
	public static final String ITEMTYPE_LEGS = "Legs";
	
	/**
	 * Placeholder constructor.
	 */
	public Legs(String ID, String name, String description, int grade, int maxDurability, int durebility, float physicalDef, float magicDef, float defM) {
		super(ID, name, description, grade, maxDurability, durebility, physicalDef, magicDef, defM);
		addItemType(ITEMTYPE_LEGS);
	}

}

package com.mygdx.inventory_test.inventory_system.item.equippable;

import com.mygdx.inventory_test.inventory_system.item.Item;

public abstract class Equippable extends Item {
	
	// Item types
	public static final String ITEMTYPE_EQUIPPABLE = "Equippable";
	
	// Other variables
	private float attackSpeed;
	
	// Damage variables
	private float physicalDmg;
	private float magicDmg;
	private float dmgM;
	private float critDmg;
	private float critDmgM;
	private float critChance;
	private float fireDmg;
	private float waterDmg;
	private float iceDmg;
	private float windDmg;
	private float earthDmg;
	private float holyDmg;
	private float darknessDmg;
	private float lightningDmg;
	private float illusionDmg;
	
	// Defense variables
	private float physicalDef;
	private float magicDef;
	private float defM;
	private float fireRes;
	private float waterRes;
	private float iceRes;
	private float windRes;
	private float earthRes;
	private float holyRes;
	private float darknessRes;
	private float lightningRes;
	private float illusionRes;
	
	/**
	 * Constructor to be used for weapons.
	 * 
	 * @param ID			The ID of this item.
	 * @param name			The name of this item.
	 * @param description	The description of this item.
	 * @param grade			The grade of this item.
	 * @param maxDurability	The max durability of this item.
	 * @param durebility	The current durability of this item.
	 * @param physicalDmg	The physical damage this item does.
	 * @param magicDmg		The magic damage this item does.
	 * @param dmgM			The damage multiplier.
	 * @param critDmg		The critical damage this item does.
	 * @param critDmgM		The critical damage multiplier.
	 * @param critChance	The chance of a critical hit ocuring.
	 */
	public Equippable(String ID, String name, String description, int grade, int maxDurability, int durebility, float physicalDmg, float magicDmg, float dmgM, float critDmg, float critDmgM, float critChance) {
		super(ID, name, description, grade, maxDurability, durebility, 1, 1);
		setPhysicalDmg(physicalDmg);
		setMagicDmg(magicDmg);
		setDmgM(dmgM);
		setCritDmg(critDmg);
		setCritDmgM(critDmgM);
		setCritChance(critChance);
		addItemType(ITEMTYPE_EQUIPPABLE);
	}
	
	/**
	 * Constructor to be used for defensive items.
	 * 
	 * @param ID			The ID of this item.
	 * @param name			The name of this item.
	 * @param description	The description of this item.
	 * @param grade			The grade of this item.
	 * @param maxDurability	The max durability of this item.
	 * @param durebility	The current durability of this item.
	 * @param physicalDef	The physical defense of this item.
	 * @param magicDef		The magic defense of this item.
	 * @param defM			The defense multiplier.
	 */
	public Equippable(String ID, String name, String description, int grade, int maxDurability, int durebility, float physicalDef, float magicDef, float defM) {
		super(ID, name, description, grade, maxDurability, durebility, 1, 1);
		setPhysicalDef(physicalDef);
		setMagicDef(magicDef);
		setDefM(defM);
		addItemType(ITEMTYPE_EQUIPPABLE);
	}
	
	/*
	 * Damage getters and setters.
	 */
	public float getAttackSpeed() {
		return attackSpeed;
	}

	public void setAttackSpeed(float attackSpeed) {
		this.attackSpeed = attackSpeed;
	}
	public float getPhysicalDmg() {
		return physicalDmg;
	}

	public void setPhysicalDmg(float dmg) {
		this.physicalDmg = dmg;
	}

	public float getMagicDmg() {
		return magicDmg;
	}

	public void setMagicDmg(float magicDmg) {
		this.magicDmg = magicDmg;
	}

	public float getDmgM() {
		return dmgM;
	}

	public void setDmgM(float multipier) {
		this.dmgM = multipier;
	}
	
	public float getCritDmg() {
		return critDmg;
	}

	public void setCritDmg(float critDmg) {
		this.critDmg = critDmg;
	}

	public float getCritDmgM() {
		return critDmgM;
	}

	public void setCritDmgM(float cDmgM) {
		this.critDmgM = cDmgM;
	}

	public float getCritChance() {
		return critChance;
	}

	public void setCritChance(float cChance) {
		this.critChance = cChance;
	}

	public float getFireDmg() {
		return fireDmg;
	}

	public void setFireDmg(float fireDmg) {
		this.fireDmg = fireDmg;
	}

	public float getWaterDmg() {
		return waterDmg;
	}

	public void setWaterDmg(float waterDmg) {
		this.waterDmg = waterDmg;
	}

	public float getIceDmg() {
		return iceDmg;
	}

	public void setIceDmg(float iceDmg) {
		this.iceDmg = iceDmg;
	}

	public float getWindDmg() {
		return windDmg;
	}

	public void setWindDmg(float windDmg) {
		this.windDmg = windDmg;
	}

	public float getEarthDmg() {
		return earthDmg;
	}

	public void setEarthDmg(float earthDmg) {
		this.earthDmg = earthDmg;
	}

	public float getHolyDmg() {
		return holyDmg;
	}

	public void setHolyDmg(float holyDmg) {
		this.holyDmg = holyDmg;
	}

	public float getDarknessDmg() {
		return darknessDmg;
	}

	public void setDarknessDmg(float darknessDmg) {
		this.darknessDmg = darknessDmg;
	}

	public float getLightningDmg() {
		return lightningDmg;
	}

	public void setLightningDmg(float lightningDmg) {
		this.lightningDmg = lightningDmg;
	}

	public float getIllusionDmg() {
		return illusionDmg;
	}

	public void setIllusionDmg(float illusionDmg) {
		this.illusionDmg = illusionDmg;
	}
	
	public float getItemDmg() {
		return getPhysicalDmg() * getDmgM();
		
	}
	
	/*
	 * Defense getters and setters.
	 */
	public float getPhysicalDef() {
		return physicalDef;
	}

	public void setPhysicalDef(float physicalDef) {
		this.physicalDef = physicalDef;
	}

	public float getMagicDef() {
		return magicDef;
	}

	public void setMagicDef(float magicDef) {
		this.magicDef = magicDef;
	}

	public float getDefM() {
		return defM;
	}

	public void setDefM(float defM) {
		this.defM = defM;
	}

	public float getFireRes() {
		return fireRes;
	}

	public void setFireRes(float fireRes) {
		this.fireRes = fireRes;
	}

	public float getWaterRes() {
		return waterRes;
	}

	public void setWaterRes(float waterRes) {
		this.waterRes = waterRes;
	}

	public float getIceRes() {
		return iceRes;
	}

	public void setIceRes(float iceRes) {
		this.iceRes = iceRes;
	}

	public float getWindRes() {
		return windRes;
	}

	public void setWindRes(float windRes) {
		this.windRes = windRes;
	}

	public float getEarthRes() {
		return earthRes;
	}

	public void setEarthRes(float earthRes) {
		this.earthRes = earthRes;
	}

	public float getHolyRes() {
		return holyRes;
	}

	public void setHolyRes(float holyRes) {
		this.holyRes = holyRes;
	}

	public float getDarknessRes() {
		return darknessRes;
	}

	public void setDarknessRes(float darknessRes) {
		this.darknessRes = darknessRes;
	}

	public float getLightningRes() {
		return lightningRes;
	}

	public void setLightningRes(float lightningRes) {
		this.lightningRes = lightningRes;
	}

	public float getIllusionRes() {
		return illusionRes;
	}

	public void setIllusionRes(float illusionRes) {
		this.illusionRes = illusionRes;
	}

}

package com.mygdx.inventory_test.inventory_system.item.equippable.weapon.dagger;

import com.mygdx.inventory_test.inventory_system.item.equippable.weapon.Weapon;

public abstract class Dagger extends Weapon {
	
	// Item types
	public static final String ITEMTYPE_DAGGER = "Dagger";
	
	/**
	 * Placeholder constructor.
	 */
	public Dagger(String ID, String name, String description, int grade, int maxDurability, int durebility, float physicalDmg, float magicDmg, float critDmg) {
		super(ID, name, description, grade, maxDurability, durebility, physicalDmg, magicDmg, 1.8f, critDmg, 2.5f, 0.45f);
		addItemType(ITEMTYPE_DAGGER);
	}

}

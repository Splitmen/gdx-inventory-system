package com.mygdx.inventory_test.inventory_system.item.equippable.ranged_weapon.shortbow;

import com.mygdx.inventory_test.inventory_system.item.equippable.ranged_weapon.RangedWeapon;

public abstract class Shortbow extends RangedWeapon {
	
	// Item types
	public static final String ITEMTYPE_SHORTBOW = "Shortbow";
	
	/**
	 * Placeholder constructor.
	 */
	public Shortbow(String ID, String name, String description, int grade, int maxDurability, int durebility, float physicalDmg, float magicDmg, float dmgM, float critDmg, float critDmgM, float critChance, float rSpeed, float range) {
		super(ID, name, description, grade, maxDurability, durebility, physicalDmg, magicDmg, dmgM, critDmg, critDmgM, critChance, rSpeed, range);
		addItemType(ITEMTYPE_SHORTBOW);
	}

}

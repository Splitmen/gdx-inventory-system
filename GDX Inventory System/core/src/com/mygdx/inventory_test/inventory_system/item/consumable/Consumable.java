package com.mygdx.inventory_test.inventory_system.item.consumable;

import com.mygdx.inventory_test.inventory_system.item.Item;

public abstract class Consumable extends Item {
	
	// Item types
	public static final String ITEMTYPE_CONSUMABLE = "Consumable";
	
	/**
	 * Placeholder constructor.
	 */
	public Consumable(String ID, String name, String description, int grade, int maxStackSize, int stackSize) {
		super(ID, name, description, grade, -1, -1, maxStackSize, stackSize);
		addItemType(ITEMTYPE_CONSUMABLE);
	}
	
	/**
	 * Called when this consumable is consumed.
	 */
	public abstract void consume();

}

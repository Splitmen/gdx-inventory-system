package com.mygdx.inventory_test.inventory_system.item.equippable.weapon.battle_axe;

import com.mygdx.inventory_test.inventory_system.item.equippable.weapon.Weapon;

public abstract class BattleAxe extends Weapon {
	
	// Item types
	public static final String ITEMTYPE_BATTLE_AXE = "Battle Axe";
	
	/**
	 * Placeholder constructor.
	 */
	public BattleAxe(String ID, String name, String description, int grade, int maxDurability, int durebility, float physicalDmg, float magicDmg, float dmgM, float critDmg, float critDmgM, float critChance) {
		super(ID, name, description, grade, maxDurability, durebility, physicalDmg, magicDmg, dmgM, critDmg, critDmgM, critChance);
		addItemType(ITEMTYPE_BATTLE_AXE);
	}

}

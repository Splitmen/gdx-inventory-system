package com.mygdx.inventory_test.inventory_system.exceptions;

/**
 * Thrown to indicate that an item index of an inventory is out of range.
 * 
 * @author Benedict
 *
 */
public class InventoryOutOfBoundsException extends IndexOutOfBoundsException {
	
	private static final long serialVersionUID = 2915974699780412630L;

	public InventoryOutOfBoundsException() {
	}

	public InventoryOutOfBoundsException(String s) {
		super(s);
	}

}

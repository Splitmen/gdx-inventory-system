package com.mygdx.inventory_test.inventory_system;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.mygdx.inventory_test.inventory_system.item.Item;

public class Slot extends Button {
	
	private Item item = null;

	public Slot(Item item) {
		setItem(item);
	}

	public Slot(Item item, Skin skin) {
		super(skin);
		setItem(item);
	}

	public Slot(Item item, ButtonStyle style) {
		super(style);
		setItem(item);
	}

	public Slot(Item item, Drawable up) {
		super(up);
		setItem(item);
	}

	public Slot(Item item, Skin skin, String styleName) {
		super(skin, styleName);
		setItem(item);
	}

	public Slot(Item item, Actor child, ButtonStyle style) {
		super(child, style);
		setItem(item);
	}

	public Slot(Item item, Drawable up, Drawable down) {
		super(up, down);
		setItem(item);
	}

	public Slot(Item item, Actor child, Skin skin) {
		super(child, skin);
		setItem(item);
	}

	public Slot(Item item, Actor child, Skin skin, String styleName) {
		super(child, skin, styleName);
		setItem(item);
	}

	public Slot(Item item, Drawable up, Drawable down, Drawable checked) {
		super(up, down, checked);
		setItem(item);
	}
	
	/*
	 * Getters and setters.
	 */
	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

}

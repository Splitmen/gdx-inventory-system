package com.mygdx.inventory_test.inventory_system;

import com.mygdx.inventory_test.inventory_system.exceptions.InventoryOutOfBoundsException;
import com.mygdx.inventory_test.inventory_system.item.Item;

public interface Inventory {
	//TODO add comments for every method
	/**
	 * Add the given item to this inventory.
	 * 
	 * @param item	The item to be added.
	 * @return		the amount that could not be added.
	 */
	public abstract int add(Item item);
	public abstract int add(Item item, int amount);
	public abstract int addAtIndex(int index, Item item);
	public abstract int addAtIndex(int index, Item item, int amount);
	public abstract int remove(Item item, int amount);
	public abstract int removeAtIndex(int index, int amount);
	public abstract Item cut(Item item, int amount);
	public abstract Item cutAtIndex(int index, int amount);
	public abstract boolean set(int index, Item item);
	public abstract boolean set(int index, Item item, int amount);
	
	public abstract boolean isInvSlotsFull();
	public abstract int getAmountInInvOf(Item item);
	public abstract Item getItem(int index) throws InventoryOutOfBoundsException;
	public abstract int getIndexOf(Item item, boolean returnOrder);
	public abstract int getIndexOfItemStackNotFull(Item item, boolean returnOrder);
	public abstract int getAmountOfItemsStacksNotFull(Item item);
	
	public abstract int getMaxInvSize();
	public abstract void setMaxInvSize(int maxInvSize);
	public abstract String getID();
	public abstract void setID(String iD);
	public abstract String getName();
	public abstract void setName(String name);
	
	public abstract void addMaxInvSize(int amount);
	public abstract void removeMaxInvSize(int amount);

}

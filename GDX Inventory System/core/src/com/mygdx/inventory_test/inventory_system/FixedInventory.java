package com.mygdx.inventory_test.inventory_system;

import java.util.ArrayList;
import java.util.List;

import com.mygdx.inventory_test.inventory_system.exceptions.InventoryOutOfBoundsException;
import com.mygdx.inventory_test.inventory_system.item.Item;

/**
 * 
 * 
 * @author Benedict
 *
 */
public class FixedInventory implements Inventory {
	
	private List<Slot> invList = new ArrayList<Slot>();// The list with all the slots that contain the items in this inventory
	protected int maxInvSize;
	private String ID;
	private String name;
	
	/**
	 * Simple constructor.
	 */
	public FixedInventory(String ID, int invSize) {
		setID(ID);
		setName(ID);
		setMaxInvSize(invSize);
	}
	
	/**
	 * Full constructor.
	 */
	public FixedInventory(String ID, String name, int invSize) {
		setID(ID);
		setName(name);
		setMaxInvSize(invSize);
	}
	
	/*
	 * The inventory manipulation methods.
	 */
	/**
	 * Adds the given item to this inventory.
	 * It adds whatever fits in the inventory and returns the stack amount of the given item that did not fit in this inventory.
	 * 
	 * @param item		The item to add to this inventory.
	 * @return			the stack amount of the given item that could not be added.<br>
	 * 					So if 0 is returned then the given item was successfully added to this inventory.
	 */
	public int add(Item item) {
		int result = 0;
		int currentItemIndex;
		if(null != item) {
			int amount = item.getStackSize();
			Math.abs(amount);
			if(amount > 0) {
				while(amount > 0) {
					currentItemIndex = getIndexOfItemStackNotFull(item, true);
					if(currentItemIndex != -1) { amount = amount - (amount - getItem(currentItemIndex).addToStack(amount)); }
					else if(!isInvSlotsFull()) {
						try {
							Item newItem = (Item) item.clone();
							newItem.setStackSize(0);
							getInvList().get(getIndexOf(null, true)).setItem(newItem);
						} catch (CloneNotSupportedException e) {
							e.printStackTrace();
						}
					} else {
						result = amount;
						break;
					}
					
				}
			} else {
				result = 0;
			}
		} else {
			System.out.println("Could not add the item because the given item is null.");
		}
		return result;
	}
	
	/**
	 * Adds the given item with the specified amount to this inventory.
	 * It adds whatever fits in the inventory and returns the amount of the given item that did not fit in this inventory.<br>
	 * So if 0 is returned then the given item with the specified amount fits inside of the inventory and thus was successfully added.
	 * 
	 * @param item		The item to add to this inventory.
	 * @param amount	The amount of the given item to add to this inventory.
	 * @return			the amount of the given item that could not be added to this inventory.
	 */
	public int add(Item item, int amount) {
		Math.abs(amount);
		int result = 0;
		int currentItemIndex;
		if(amount > 0) {
			while(amount > 0) {
				currentItemIndex = getIndexOfItemStackNotFull(item, true);
				if(currentItemIndex != -1) { amount = amount - (amount - getItem(currentItemIndex).addToStack(amount)); }
				else if(!isInvSlotsFull()) {
					try {
						Item newItem = (Item) item.clone();
						newItem.setStackSize(0);
						getInvList().get(getIndexOf(null, true)).setItem(newItem);
					} catch (CloneNotSupportedException e) {
						e.printStackTrace();
					}
				} else {
					result = amount;
					break;
				}
				
			}
		} else {
			result = 0;
		}
		return result;
	}
	
	/**
	 * Adds the given item at the given index in this inventory.
	 * If the given item and the item at the given index have the same name and ID,<br>
	 * then it tries to add the given item's stack size to the item's stack size at the given index.
	 * <dl>
	 * Returns the amount that exceeds the max stack size of the given item<br>
	 * after adding both the item at the given index and the given item's stack size.<br>
	 * Even if the whole stack of the given item can not be added, whatever can be added is added to this inventory.
	 * </dl>
	 * @param index	The index of where the given item is to be added.
	 * @param item	The item to put in the inventory.
	 * @return		the stack amount that did not fit in this inventory at the given index, return -1 if argument item is null.
	 */
	public int addAtIndex(int index, Item item) {
		int result = 0;
		if(null != item) {
			try {
				if(null == getItem(index)) {
					try {
						getInvList().get(index).setItem((Item) item.clone());
					} catch (CloneNotSupportedException e) {
						e.printStackTrace();
					} catch (NullPointerException e) {
						e.printStackTrace();
					}
				} else if(getItem(index).getID().equals(item.getID()) && getItem(index).getName().equals(item.getName())) {
					result = getItem(index).addToStack(item.getStackSize());
				}
			} catch (InventoryOutOfBoundsException e) {
				System.out.println("Could not add the item because of the index being out of range.");
			}
		} else {
			System.out.println("Could not add the item because the item is null");
			result = -1;
		}
		return result;
		
	}
	
	/**
	 * Adds the given item at the given index in this inventory.
	 * If the given item and the item at the given index have the same name and ID,<br>
	 * then it tries to add the given item's stack size to the item's stack size at the given index.
	 * <dl>
	 * Returns the amount that exceeds the max stack size of the given item<br>
	 * after adding both the item at the given index and the given item's stack size.<br>
	 * Even if the whole stack of the given item can not be added, whatever can be added is added to this inventory.
	 * </dl>
	 * @param index		The index of where the given item is to be added.
	 * @param item		The item to put in the inventory.
	 * @param amount	The amount of the given item to add to the given index.
	 * @return			the stack amount that did not fit in this inventory at the given index.
	 */
	public int addAtIndex(int index, Item item, int amount) {
		int result = 0;
		try {
			if(getItem(index) == null) {
				try {
					Item newItem = (Item) item.clone();
					result = newItem.setStackSize(amount);
					getInvList().get(index).setItem(newItem);
				} catch (CloneNotSupportedException e) {
					e.printStackTrace();
				}
			} else if(getItem(index).getID().equals(item.getID()) && getItem(index).getName().equals(item.getName())) {
				result = getItem(index).addToStack(amount);
			}
		} catch (InventoryOutOfBoundsException e) {
			System.out.println("Could not add the item because of the index being out of range.");
		}
		return result;
		
	}
	
	/**
	 * Removes the specified amount of the given item from this inventory.
	 * 
	 * @param item		The item to remove from this inventory.
	 * @param amount	The amount of the given item to remove.
	 * @return			the stack amount of the specified item that could not be removed in this inventory.<br>
	 * 					So returns 0 if the specified amount of the given item could be removed from this inventory.
	 */
	public int remove(Item item, int amount) {
		Math.abs(amount);
		int result = 0;
		int currentItemIndex;
		if(amount > 0) {
			while(amount > 0) {
				currentItemIndex = getIndexOf(item, false);
				if(currentItemIndex != -1) {
					if(getItem(currentItemIndex).getStackSize() <= amount) {
						amount = amount - getItem(currentItemIndex).getStackSize();
						getInvList().set(currentItemIndex, null);
					} else {
						getItem(currentItemIndex).removeFromStack(amount);
						amount = 0;
					}
				} else {
					result = amount;
					break;
				}
				
			}
		} else {
			result = 0;
		}
		return result;
	}
	
	/**
	 * Removes the given amount off of the item at the given index in this inventory.
	 * 
	 * @param index		The index of where to remove the given item from this inventory.
	 * @param amount 	The amount to remove from the item at the given index.
	 * @return			the amount that amount exceeds the stack size of the item at the given index.
	 */
	public int removeAtIndex(int index, int amount) {
		int result = 0;
		try {
			if(null != getItem(index)) {
				result = getItem(index).removeFromStack(amount);
				if(result > 0 || getItem(index).getStackSize() == 0) {
					getInvList().set(index, null);
				}
			} else {
				result = amount;
			}
		} catch (InventoryOutOfBoundsException e) {
			System.out.println("Could not remove the item because of the index being out of range.");
		}
		return result;
	}
	
	/**
	 * Cut the given item with the specified amount in this inventory.<br>
	 * If the specified amount is bigger then the amount to remove it will only cut what it can.
	 * <dl>
	 * Postcondition: The item returned can have a stackSize greater then its maxStackSize variable.
	 * </dl>
	 * @param item		The item to search for in the inventory and then cut.
	 * @param amount	The specified stack size of the given item to cut.
	 * @return			the cut item that can have a stackSize bigger then its own maxStackSize, returns null if the casting to Item fails.
	 */
	public Item cut(Item item, int amount) {
		Math.abs(amount);
		Item newItem = null;
		try {
			newItem = (Item) item.clone();
			newItem.setStackSize(amount - remove(item, amount));
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return newItem;
	}
	
	/**
	 * Cuts the given amount from the item at the given index.
	 * 
	 * @param index		The index of the item to cut and return.
	 * @param amount	The stack amount to cut from the item at the given index.
	 * @return			the cut item, returns null if the item at the given index is null.
	 */
	public Item cutAtIndex(int index, int amount) {
		Math.abs(amount);
		Item newItem = null;
		try {
			if(null != getItem(index)) {
				newItem = (Item) getItem(index).clone();
				newItem.setStackSize(amount - removeAtIndex(index, amount));
			}
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		} catch (InventoryOutOfBoundsException e) {
			System.out.println("Could not cut the item because of the index being out of range.");
		}
		return newItem;
	}
	
	/**
	 * Sets the given item to the specified index in this inventory.<br>
	 * If there is already an item at that index the item is overridden.
	 * 
	 * @param index	The index of where to set the given item in this inventory.
	 * @param item	The item to set at the specified index.
	 * @return		true if the item was successfully set in the inventory and amount > 0, return false otherwise.
	 */
	public boolean set(int index, Item item) {
		boolean result = false;
		if(index < 0 || index >= getMaxInvSize()) {
			System.out.println("Could not set the item due to the index being out of range.");
		} else {
			try {
				getInvList().get(index).setItem((Item) item.clone());
				result = true;
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			} catch (NullPointerException e) {
				getInvList().set(index, null);
			}
		}
		return result;
	}

	/**
	 * Sets the given item to the specified index in this inventory.<br>
	 * If there is already an item at that index the item is overridden.
	 * <dl>
	 * If amount is bigger then the given item's max stack size then the stack size of the set item will be the max stack size of that item.
	 * </dl>
	 * @param index		The index of where to set the given item in this inventory.
	 * @param item		The item to set at the specified index.
	 * @param amount	The stack size of the item to set.
	 * @return			true if the item was successfully set in the inventory and amount > 0, return false otherwise.
	 */
	public boolean set(int index, Item item, int amount) {
		boolean result = false;
		if(index < 0 || index >= getMaxInvSize()) {
			System.out.println("Could not set the item due to the index being out of range.");
		} else {
			try {
				if(amount > 0) {
					Item newItem = (Item) item.clone();
					if(amount <= item.getMaxStackSize()) newItem.setStackSize(amount);
					else newItem.setStackSize(item.getMaxStackSize());
					getInvList().get(index).setItem(newItem);
					result = true;
				} else {
					result = false;
				}
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
	
	/*
	 * The information helper methods.
	 */
	/**
	 * Checks if this inventory has no empty item slots left.<br>
	 * 
	 * @return true if all inventory slots are full, false otherwise.
	 */
	public boolean isInvSlotsFull() {
		boolean result = true;
		for (int i = 0; i < getMaxInvSize(); i++) {
			if(getItem(i) == null) result = false;
			
		}
		return result;
	}

	/**
	 * Goes through the inventory, finds all the items that are equal to the given item<br>
	 * and add's up all of their stack sizes, which is then returned.
	 * 
	 * @param item	The item to search for and add up all the stack sizes from.
	 * @return		the total amount of all stack sizes combined that are in the inventory of the given item.
	 */
	public int getAmountInInvOf(Item item) {
		int result = 0;
		for (int i = 0; i < getMaxInvSize(); i++) {
			if(getItem(i) != null && getItem(i).getID().equals(item.getID()) && getItem(i).getName().equals(item.getName())) {
				result = result + getItem(i).getStackSize();
			}
			
		}
		return result;
	}
	
	/**
	 * Returns the item in the given index of this inventory.<br>
	 * 
	 * @param index								The index of this inventory to return the item from.
	 * @return									the item in the given index.
	 * @throws InventoryOutOfBoundsException	if the index is out of range (<code>(index < 0 || index >= getMaxInvSize())</code>)
	 */
	public Item getItem(int index) throws InventoryOutOfBoundsException {
		Math.abs(index);
		if(index < 0 || index >= getMaxInvSize()) {
			throw new InventoryOutOfBoundsException();
		}
		return getInvList().get(index).getItem();
	}
	
	/**
	 * This method returns the index position of the given item in this inventory<br>
	 * If null is given as argument the it searches for an empty slot in the inventory.
	 * <br>
	 * The returnOrder argument dictates if this method returns the first item it finds or the last,<br>
	 * true for the first and false for the last.
	 * <dl>
	 * It returns the item if these criteria are met:
	 * <ol>
	 * 		(<code>found_item_ID.equals(given_item_ID)</code>) is true.<br>
	 * 		(<code>found_item_name.equals(given_item_name)</code>) is true.
	 * </ol>
	 * 
	 * @param item			The item to find the index of.
	 * @param returnOrder	Set to true to return the index of the first item found, and false for the last.
	 * @return				the index of the given item if found, otherwise returns -1.
	 */
	public int getIndexOf(Item item, boolean returnOrder) {
		int result = -1;
		for (int i = 0; i < getMaxInvSize(); i++) {
			if(item == null) {
				if(getItem(i) == null) {
					result = i;
					if(returnOrder) break;
				}
			} else if(getItem(i) != null && getItem(i).getID().equals(item.getID()) && getItem(i).getName().equals(item.getName())) {
				result = i;
				if(returnOrder) break;
			}
		}
		return result;
	}
	
	/**
	 * This method returns the index position of the given item in this inventory<br>
	 * if and only if (<code>found_item.stackSize < found_item.maxStackSize</code>)<br>
	 * <dl>
	 * It returns the item index only if these criteria are met:
	 * <ol>
	 * 		(<code>found_item_ID.equals(given_item_ID)</code>) is true.<br>
	 * 		(<code>found_item_name.equals(given_item_name)</code>) is true.<br>
	 * 		(<code>found_item.stackSize < found_item.maxStackSize</code>) is true.
	 * </ol>
	 * The returnOrder argument dictates if this method returns the first item it finds or the last,<br>
	 * true for the first and false for the last.
	 * </dl>
	 * 
	 * @param item			The item to search for in the inventory and check if found_item.stackSize < found_item.maxStackSize.
	 * @param returnOrder	Set to true to return the index of the first item found, and false for the last.
	 * @return				the index of the first or last item that meets all the conditions, otherwise returns -1.
	 */
	public int getIndexOfItemStackNotFull(Item item, boolean returnOrder) {
		int result = -1;
		for (int i = 0; i < getMaxInvSize(); i++) {
			if(null != getItem(i) && getItem(i).getID().equals(item.getID()) && getItem(i).getName().equals(item.getName()) && getItem(i).getStackSize() < getItem(i).getMaxStackSize()) {
				result = i;
				if(returnOrder) break;
			}
			
		}
		return result;
	}
	
	/**
	 * Returns the total amount of free space of all items stacks that are the same item as the given item with a stack size bigger then 0.
	 * 
	 * @param item	The item to search for.
	 * @return		the amount of total free space in stacks that have a stack size bigger then 0 and where they are equals to the given item.
	 */
	public int getAmountOfItemsStacksNotFull(Item item) {
		int result = 0;
		for (int i = 0; i < getInvList().size(); i++) {
			if(getItem(i) != null && getItem(i).getID().equals(item.getID()) && getItem(i).getName().equals(item.getName()) && getItem(i).getStackSize() < getItem(i).getMaxStackSize()) {
				result = result + (getItem(i).getMaxStackSize() - getItem(i).getStackSize());
			}
			
		}
		return result;
	}
	
	/*
	 * Getters and setters.
	 */
	private List<Slot> getInvList() {
		return invList;
	}

	public int getMaxInvSize() {
		return maxInvSize;
	}
	
	public void setMaxInvSize(int maxInvSize) {
		if(this.maxInvSize < maxInvSize) {
			addMaxInvSize(maxInvSize - this.maxInvSize);
		} else if(this.maxInvSize > maxInvSize) {
			removeMaxInvSize(this.maxInvSize - maxInvSize);
		}
		this.maxInvSize = maxInvSize;
	}

	public String getID() {
		return ID;
	}

	public void setID(String iD) {
		ID = iD;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	/*
	 * Adders and remover's.
	 */
	public void addMaxInvSize(int amount) {
		this.maxInvSize = maxInvSize + amount;
		for (int i = 0; i < amount; i++) {
			getInvList().add(new Slot(null));
			
		}
	}
	
	public void removeMaxInvSize(int amount) {
		this.maxInvSize = maxInvSize - amount;
		for (int i = 0; i < amount; i++) {
			getInvList().remove(this.getInvList().size() - 1);
			
		}
	}

}

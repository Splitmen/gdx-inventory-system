package com.mygdx.inventory_test.inventory_list_system.item.equippable.weapon;

import com.mygdx.inventory_test.inventory_list_system.item.equippable.Equippable;

public abstract class Weapon extends Equippable {
	
	// Item types
	public static final String ITEMTYPE_WEAPON = "Weapon";
	
	/**
	 * Placeholder constructor.
	 */
	public Weapon(String ID, String name, String description, int grade, int maxDurability, int durebility, float physicalDmg, float magicDmg, float dmgM, float critDmg, float critDmgM, float critChance) {
		super(ID, name, description, grade, maxDurability, durebility, physicalDmg, magicDmg, dmgM, critDmg, critDmgM, critChance);
		addItemType(ITEMTYPE_WEAPON);
	}

}

package com.mygdx.inventory_test.inventory_list_system.item;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

public abstract class Item implements Cloneable {
	
	// Item types
	public static final String ITEMTYPE_MISC = "Misc";
	public static final String ITEMTYPE_UNIQUE = "Unique";
	public static final String ITEMTYPE_MATERIAL = "Material";
	public static final String ITEMTYPE_INGOT = "Ingot";
	public static final String ITEMTYPE_DEBUG = "Debug";
	
	// Item grade constants
	public static final int NORMAL = 100;
	public static final int COMMON = 101;
	public static final int MEDIUM = 102;
	public static final int SPECIAL = 103;
	public static final int EPIC = 104;
	public static final int UNIQUE = 105;
	public static final int LEGENDARY = 106;
	public static final int MYTHOLOGICAL = 107;
	public static final int ANCIENT = 108;
	
	// Item variables
	private Image icon;
	private String ID;
	private String name;
	private String description;
	private String itemTypes;	//The item types this item possesses
	private int grade;
	private int maxDurability;
	private int durability;
	private int maxStackSize;
	private int stackSize;
	
	/**
	 * Basic constructor.
	 * 
	 * @param icon			The icon displayed when in an inventory.
	 * @param ID			The ID of this item.
	 * @param name			The name of this item.
	 * @param maxStackSize	The max stack size of this item.
	 */
	public Item(Image icon, String ID, String name, int maxStackSize, int stackSize) {
		maxStackSize = Math.abs(maxStackSize);
		stackSize = Math.abs(stackSize);
		icon.setWidth(64);
		icon.setHeight(64);
		setIcon(icon);
		setID(ID);
		setName(name);
		setDescription("");
		setItemTypes("");
		setGrade(100);
		setMaxDurability(-1);
		setDurability(-1);
		setMaxStackSize(maxStackSize);
		if(stackSize <= maxStackSize) setStackSize(stackSize); else setStackSize(maxStackSize);
	}
	
	/**
	 * Basic constructor.
	 * 
	 * @param ID			The ID of this item.
	 * @param name			The name of this item.
	 * @param maxStackSize	The max stack size of this item.
	 */
	public Item(String ID, String name, int maxStackSize, int stackSize) {
		maxStackSize = Math.abs(maxStackSize);
		stackSize = Math.abs(stackSize);
		setIcon(new Image(new Texture(Gdx.files.internal("no_texture.png"))));
		icon.setWidth(64);
		icon.setHeight(64);
		setID(ID);
		setName(name);
		setDescription("");
		setItemTypes("");
		setGrade(100);
		setMaxDurability(-1);
		setDurability(-1);
		setMaxStackSize(maxStackSize);
		if(stackSize <= maxStackSize) setStackSize(stackSize); else setStackSize(maxStackSize);
	}
	
	/**
	 * Constructor with every possible options.<br>
	 * Constructor used by child classes.
	 * 
	 * @param icon			The icon displayed when in an inventory.
	 * @param ID			The ID of this item.
	 * @param name			The name of this item.
	 * @param description	The description of this item.
	 * @param itemTypes		The item types this item possesses.
	 * @param grade			The item grade of this item.
	 * @param maxDurability	The maximum durability of this item.
	 * @param durebility	The current durability of this item.
	 * @param maxStackSize	The maximum stack size of this item.
	 * @param stackSize		The current stack size of this item.
	 */
	public Item(Image icon, String ID, String name, String description, int grade, int maxDurability, int durebility, int maxStackSize, int stackSize) {
		maxStackSize = Math.abs(maxStackSize);
		stackSize = Math.abs(stackSize);
		icon.setWidth(64);
		icon.setHeight(64);
		setIcon(icon);
		setID(ID);
		setName(name);
		setDescription(description);
		setItemTypes("");
		setGrade(grade);
		setMaxDurability(maxDurability);
		setDurability(durebility);
		setMaxStackSize(maxStackSize);
		if(stackSize <= maxStackSize) setStackSize(stackSize); else setStackSize(maxStackSize);
	}
	
	/**
	 * Constructor with every possible options.<br>
	 * Constructor used by child classes.
	 * 
	 * @param ID			The ID of this item.
	 * @param name			The name of this item.
	 * @param description	The description of this item.
	 * @param itemTypes		The item types this item possesses.
	 * @param grade			The item grade of this item.
	 * @param maxDurability	The maximum durability of this item.
	 * @param durebility	The current durability of this item.
	 * @param maxStackSize	The maximum stack size of this item.
	 * @param stackSize		The current stack size of this item.
	 */
	public Item(String ID, String name, String description, int grade, int maxDurability, int durebility, int maxStackSize, int stackSize) {
		maxStackSize = Math.abs(maxStackSize);
		stackSize = Math.abs(stackSize);
		setIcon(new Image(new Texture(Gdx.files.internal("no_texture.png"))));
		icon.setWidth(64);
		icon.setHeight(64);
		setID(ID);
		setName(name);
		setDescription(description);
		setItemTypes("");
		setGrade(grade);
		setMaxDurability(maxDurability);
		setDurability(durebility);
		setMaxStackSize(maxStackSize);
		if(stackSize <= maxStackSize) setStackSize(stackSize); else setStackSize(maxStackSize);
	}
	
	/**
	 * Returns how much space this item has left in it's stack.
	 * 
	 * @return	the amount of empty space in the stack of this item.
	 */
	public int emptyStackSpace() {
		return getMaxStackSize() - getStackSize();
	}
	
	/**
	 * The clone method.
	 */
	public Object clone() throws CloneNotSupportedException{
		return super.clone();
	}

	/*
	* Getters and setters.
	*/
	public Image getIcon() {
		return icon;
	}

	public void setIcon(Image icon) {
		this.icon = icon;
	}
	public String getID() {
		return ID;
	}

	public void setID(String ID) {
		this.ID = ID;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	public String getItemTypes() {
		return itemTypes;
	}

	protected void setItemTypes(String type) {
		this.itemTypes = type;
	}
	
	/**
	 * Checks if this item has the given item type.
	 * 
	 * @param type	The given item type.
	 * @return		true if this item has the given item type, otherwise return false.
	 */
	public boolean hasItemType(String type) {
		String[] vars = getItemTypes().split(" : ");
		boolean result = false;
		for (int i = 0; i < vars.length; i++) {
			if(vars[i].equals(type)) result = true;
			
		}
		return result;
	}
	

	public int getGrade() {
		return grade;
	}

	public String getGradeString() {
		switch (getGrade()) {
		default:
			return "Normal";
		case COMMON:
			return "Common";
		case MEDIUM:
			return "Medium";
		case SPECIAL:
			return "Special";
		case EPIC:
			return "Epic";
		case UNIQUE:
			return "Unique";
		case LEGENDARY:
			return "Legendary";
		case MYTHOLOGICAL:
			return "Mythological";
		case ANCIENT:
			return "Ancient";
		}
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

	public void setGradeString(String grade) {
		if(grade.equals("Common")) setGrade(101);
		else if(grade.equals("Medium")) setGrade(102);
		else if(grade.equals("Special")) setGrade(103);
		else if(grade.equals("Epic")) setGrade(104);
		else if(grade.equals("Unique")) setGrade(105);
		else if(grade.equals("Legendary")) setGrade(106);
		else if(grade.equals("Mythological")) setGrade(107);
		else if(grade.equals("Ancient")) setGrade(108);
		else setGrade(100);
	}

	public int getMaxDurability() {
		return maxDurability;
	}

	public void setMaxDurability(int maxDurability) {
		this.maxDurability = maxDurability;
	}

	public int getDurability() {
		return durability;
	}

	public void setDurability(int amount) {
		this.durability = amount;
	}

	public int getMaxStackSize() {
		return maxStackSize;
	}

	public void setMaxStackSize(int maxStackSize) {
		this.maxStackSize = maxStackSize;
	}

	public int getStackSize() {
		return stackSize;
	}
	
	/**
	 * Sets the stack size of this item.<br>
	 * If the new stack size exceeds the max stack size then the stack size is set the the max stack size.
	 * 
	 * @param stackSize	The amount to set the stack size of this item to.
	 * @return			the amount that the new stackSize exceeds the max stack size of this item.
	 */
	public int setStackSize(int stackSize) {
		stackSize = Math.abs(stackSize);
		if(stackSize <= getMaxStackSize()) {
			this.stackSize = stackSize;
			 return 0;
		} else {
			this.stackSize = getMaxStackSize();
			return stackSize - getMaxStackSize();
		}
	}
	
	/*
	 * Adders and removers.
	 */
	/**
	 * Add's a itemType to this item.
	 * 
	 * @param type	The itemType to add to this item.
	 * @return		true if type was successfully added, and return false if this item already has the given type.
	 */
	public boolean addItemType(String type) {
		String[] vars = getItemTypes().split(" : ");
		boolean add = true;
		for (int i = 0; i < vars.length; i++) {
			if(vars[i].equals(type)) add = false;
			
		}
		if(add) {
			if(getItemTypes().equals("")) setItemTypes(getItemTypes().concat(type));
			else setItemTypes(getItemTypes().concat(" : " + type));
		}
		return add;
	}
	
	/**
	 * Removes a itemType  from this item.
	 * 
	 * @param type	The itemType to remove from this item.
	 * @return		true if the type was successfully removed, and return false otherwise.
	 */
	public boolean removeItemType(String type) {
		String[] vars = getItemTypes().split(" : ");
		boolean remove = false;
		for (int i = 0; i < vars.length; i++) {
			if(vars[i].equals(type)) remove = true;
			
		}
		
		if(remove) {
			String[] vars2 = new String[vars.length - 1];
			String result = "";
			if(vars2.length > 0){
				int j = 0;
				for (int i = 0; i < vars.length; i++) {
					if(!vars[i].equals(type)) { vars2[j] = vars[i]; j++; }
					
				}
				for (int i = 0; i < vars2.length; i++) {
					if(result.equals("")) {
						result = vars2[i];
					}else {
						result = result.concat(" : " + vars2[i]);
					}
					
				}
			}
			setItemTypes(result);
			
			return true;
		}else {
			return false;
		}
	}
	
	/**
	 * Adds the specified amount to this item's durability.
	 * 
	 * @param amount	The amount of durability to add to this item.
	 * @return			the amount of durability it could not add.
	 */
	public int addDurability(int amount) {
		if(getDurability() + amount <= getMaxDurability()) {
			setDurability(getDurability() + amount);
			return 0;
		} else {
			int result = getDurability() + amount - getMaxDurability();
			setDurability(getMaxDurability());
			return result;
		}
	}
	
	/**
	 * Removes the specified amount off of this item's durability.
	 * 
	 * @param amount	The amount of durability to remove off of this item.
	 * @return			the amount of durability it could not remove.
	 */
	public int removeDurability(int amount) {
		if(getDurability() - amount >= 0) {
			setDurability(getDurability() - amount);
			return 0;
		} else {
			int amountUnder0;
			if(getDurability() != -1) { 
				amountUnder0 = Math.abs(getDurability() - amount);
			} else {
				amountUnder0 = Math.abs(getDurability() - amount) - 1;
			}
			if(getMaxDurability() != -1) setDurability(0);
			return amountUnder0;
		}
	}

	/**
	 * Increases the amount in the stack by amount.
	 * 
	 * @param amount	The amount to increase the stack with.
	 * @return			the amount the stack is over the maxStackSize.
	 */
	public int addToStack(int amount) {
		Math.abs(amount);
		if(getStackSize() + amount <= getMaxStackSize()){
			setStackSize(getStackSize() + amount);
			return 0;
		}else{
			int spaceLeft = getMaxStackSize() - getStackSize();
			setStackSize(getStackSize() + spaceLeft);
			return amount - spaceLeft;
		}
	}

	/**
	 * Decreases the amount in the stack by amount.<br>
	 * If the stack's size is smaller then the specified amount then set this stack's size to 0 and return the amount that could not be removed.
	 * 
	 * @param amount	The amount to decease the stack with.
	 * @return			the amount that could not be removed from this stack.<br>
	 * 					So return 0 if the specified amount could be removed from this stack's size.
	 */
	public int removeFromStack(int amount) {
		Math.abs(amount);
		int amountUnder0;
		if(getStackSize() - amount >= 0){
			setStackSize(getStackSize() - amount);
			return 0;
		}else{
			amountUnder0 = amount - getStackSize();
			setStackSize(0);
			return amountUnder0;
		}
	}

}

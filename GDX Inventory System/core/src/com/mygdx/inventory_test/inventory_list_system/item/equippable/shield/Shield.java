package com.mygdx.inventory_test.inventory_list_system.item.equippable.shield;

import com.mygdx.inventory_test.inventory_list_system.item.equippable.Equippable;

public abstract class Shield extends Equippable {
	
	// Item types
	public static final String ITEMTYPE_SHIELD = "Shield";
	
	/**
	 * Placeholder constructor.
	 */
	public Shield(String ID, String name, String description, int grade, int maxDurability, int durebility, float physicalDef, float magicDef) {
		super(ID, name, description, grade, maxDurability, durebility, physicalDef, magicDef, 3);
		addItemType(ITEMTYPE_SHIELD);
	}

}

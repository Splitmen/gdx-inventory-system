package com.mygdx.inventory_test.inventory_list_system.item.equippable.ranged_weapon.crossbow;

import com.mygdx.inventory_test.inventory_list_system.item.equippable.ranged_weapon.RangedWeapon;

public abstract class Crossbow extends RangedWeapon {
	
	// Item types
	public static final String ITEMTYPE_CROSSBOW = "Crossbow";
	
	/**
	 * Placeholder constructor.
	 */
	public Crossbow(String ID, String name, String description, int grade, int maxDurability, int durebility, float physicalDmg, float magicDmg, float critDmg, float rSpeed, float range) {
		super(ID, name, description, grade, maxDurability, durebility, physicalDmg, magicDmg, 2.1f, critDmg, 2.3f, 0.16f, rSpeed, range);
		addItemType(ITEMTYPE_CROSSBOW);
	}

}

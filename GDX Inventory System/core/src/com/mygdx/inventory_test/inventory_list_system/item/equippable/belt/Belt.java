package com.mygdx.inventory_test.inventory_list_system.item.equippable.belt;

import com.mygdx.inventory_test.inventory_list_system.item.equippable.Equippable;

public class Belt extends Equippable {
	
	// Item types
	public static final String ITEMTYPE_BELT = "Belt";

	public Belt(String ID, String name, String description, int grade, int maxDurability, int durebility, float physicalDef, float magicDef, float defM) {
		super(ID, name, description, grade, maxDurability, durebility, physicalDef, magicDef, defM);
		addItemType(ITEMTYPE_BELT);
	}

}

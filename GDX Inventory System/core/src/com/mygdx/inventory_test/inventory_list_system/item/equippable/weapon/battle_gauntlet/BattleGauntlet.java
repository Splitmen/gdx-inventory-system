package com.mygdx.inventory_test.inventory_list_system.item.equippable.weapon.battle_gauntlet;

import com.mygdx.inventory_test.inventory_list_system.item.equippable.weapon.Weapon;

public abstract class BattleGauntlet extends Weapon {
	
	// Item types
	public static final String ITEMTYPE_BATTLE_GAUNTLET = "Battle Gauntlet";
	
	/**
	 * Placeholder constructor.
	 */
	public BattleGauntlet(String ID, String name, String description, int grade, int maxDurability, int durebility, float physicalDmg, float magicDmg, float critDmg) {
		super(ID, name, description, grade, maxDurability, durebility, physicalDmg, magicDmg, 1.2f, critDmg, 1.7f, 0.15f);
		addItemType(ITEMTYPE_BATTLE_GAUNTLET);
	}

}

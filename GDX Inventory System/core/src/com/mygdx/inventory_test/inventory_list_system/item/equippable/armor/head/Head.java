package com.mygdx.inventory_test.inventory_list_system.item.equippable.armor.head;

import com.mygdx.inventory_test.inventory_list_system.item.equippable.armor.Armor;

public abstract class Head extends Armor {
	
	// Item types
	public static final String ITEMTYPE_HEAD = "Head";
	
	/**
	 * Placeholder constructor.
	 */
	public Head(String ID, String name, String description, int grade, int maxDurability, int durebility, float physicalDef, float magicDef, float defM) {
		super(ID, name, description, grade, maxDurability, durebility, physicalDef, magicDef, defM);
		addItemType(ITEMTYPE_HEAD);
	}

}

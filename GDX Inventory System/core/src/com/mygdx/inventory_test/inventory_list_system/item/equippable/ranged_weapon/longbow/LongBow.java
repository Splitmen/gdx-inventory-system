package com.mygdx.inventory_test.inventory_list_system.item.equippable.ranged_weapon.longbow;

import com.mygdx.inventory_test.inventory_list_system.item.equippable.ranged_weapon.RangedWeapon;

public abstract class LongBow extends RangedWeapon {
	
	// Item types
	public static final String ITEMTYPE_LONGBOW = "Longbow";
	
	/**
	 * Placeholder constructor.
	 */
	public LongBow(String ID, String name, String description, int grade, int maxDurability, int durebility, float physicalDmg, float magicDmg, float dmgM, float critDmg, float critDmgM, float critChance, float rSpeed, float range) {
		super(ID, name, description, grade, maxDurability, durebility, physicalDmg, magicDmg, dmgM, critDmg, critDmgM, critChance, rSpeed, range);
		addItemType(ITEMTYPE_LONGBOW);
	}

}

package com.mygdx.inventory_test.inventory_list_system.item.equippable.weapon.mace;

import com.mygdx.inventory_test.inventory_list_system.item.equippable.weapon.Weapon;

public abstract class Mace extends Weapon {
	
	// Item types
	public static final String ITEMTYPE_MACE = "Mace";
	
	/**
	 * Placeholder constructor.
	 */
	public Mace(String ID, String name, String description, int grade, int maxDurability, int durebility, float physicalDmg, float magicDmg, float dmgM, float critDmg, float critDmgM, float critChance) {
		super(ID, name, description, grade, maxDurability, durebility, physicalDmg, magicDmg, dmgM, critDmg, critDmgM, critChance);
		addItemType(ITEMTYPE_MACE);
	}

}

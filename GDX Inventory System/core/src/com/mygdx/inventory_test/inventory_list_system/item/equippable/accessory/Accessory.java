package com.mygdx.inventory_test.inventory_list_system.item.equippable.accessory;

import com.mygdx.inventory_test.inventory_list_system.item.equippable.Equippable;

public abstract class Accessory extends Equippable {
	
	// Item types
	public static final String ITEMTYPE_ACCESSORY = "Accessory";
	
	/**
	 * Offensive accessory constructor.
	 * 
	 * @param ID			The ID of this item.
	 * @param name			The name of this item.
	 * @param description	The description of this item.
	 * @param grade			The grade of this item.
	 * @param maxDurability	The max durability of this item.
	 * @param durebility	The current durability of this item.
	 * @param physicalDmg	The physical damage this item does.
	 * @param magicDmg		The magic damage this item does.
	 * @param dmgM			The damage multiplier.
	 * @param critDmg		The critical damage this item does.
	 * @param critDmgM		The critical damage multiplier.
	 * @param critChance	The chance of a critical hit ocuring.
	 */
	public Accessory(String ID, String name, String description, int grade, int maxDurability, int durebility, float physicalDmg, float magicDmg, float dmgM, float critDmg, float critDmgM, float critChance) {
		super(ID, name, description, grade, maxDurability, durebility, physicalDmg, magicDmg, dmgM, critDmg, critDmgM, critChance);
		addItemType(ITEMTYPE_ACCESSORY);
	}
	
	/**
	 * Defensive accessory constructor.
	 * 
	 * @param ID			The ID of this item.
	 * @param name			The name of this item.
	 * @param description	The description of this item.
	 * @param grade			The grade of this item.
	 * @param maxDurability	The max durability of this item.
	 * @param durebility	The current durability of this item.
	 * @param physicalDef	The physical defense of this item.
	 * @param magicDef		The magic defense of this item.
	 * @param defM			The defense multiplier.
	 */
	public Accessory(String ID, String name, String description, int grade, int maxDurability, int durebility, float physicalDef, float magicDef, float defM) {
		super(ID, name, description, grade, maxDurability, durebility, physicalDef, magicDef, defM);
		addItemType(ITEMTYPE_ACCESSORY);
	}
	
	/**
	 * Offensive and defensive accessory constructor.
	 * 
	 * @param ID			The ID of this item.
	 * @param name			The name of this item.
	 * @param description	The description of this item.
	 * @param grade			The grade of this item.
	 * @param maxDurability	The max durability of this item.
	 * @param durebility	The current durability of this item.
	 * @param physicalDmg	The physical damage this item does.
	 * @param magicDmg		The magic damage this item does.
	 * @param dmgM			The damage multiplier.
	 * @param critDmg		The critical damage this item does.
	 * @param critDmgM		The critical damage multiplier.
	 * @param critChance	The chance of a critical hit ocuring.
	 * @param physicalDef	The physical defense of this item.
	 * @param magicDef		The magic defense of this item.
	 * @param defM			The defense multiplier.
	 */
	public Accessory(String ID, String name, String description, int grade, int maxDurability, int durebility, float physicalDmg, float magicDmg, float dmgM, float critDmg, float critDmgM, float critChance, float physicalDef, float magicDef, float defM) {
		super(ID, name, description, grade, maxDurability, durebility, physicalDmg, magicDmg, dmgM, critDmg, critDmgM, critChance);
		setPhysicalDef(physicalDef);
		setMagicDef(magicDef);
		setDefM(defM);
		addItemType(ITEMTYPE_ACCESSORY);
	}

}

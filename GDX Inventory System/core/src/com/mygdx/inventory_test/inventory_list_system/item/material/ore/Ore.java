package com.mygdx.inventory_test.inventory_list_system.item.material.ore;

import com.mygdx.inventory_test.inventory_list_system.item.Item;

public abstract class Ore extends Item {
	
	// Item types
	public static final String ITEMTYPE_ORE = "Ore";

	public Ore(String ID, String name, String description, int grade, int stackSize) {
		super(ID, name, description, grade, -1, -1, 9999, stackSize);
		addItemType(ITEMTYPE_MATERIAL);
		addItemType(ITEMTYPE_ORE);
	}

}

package com.mygdx.inventory_test.inventory_list_system.item.equippable.armor.chest;

import com.mygdx.inventory_test.inventory_list_system.item.equippable.armor.Armor;

public abstract class Chest extends Armor {
	
	// Item types
	public static final String ITEMTYPE_CHEST = "Chest";
	
	/**
	 * Placeholder constructor.
	 */
	public Chest(String ID, String name, String description, int grade, int maxDurability, int durebility, float physicalDef, float magicDef, float defM) {
		super(ID, name, description, grade, maxDurability, durebility, physicalDef, magicDef, defM);
		addItemType(ITEMTYPE_CHEST);
	}

}

package com.mygdx.inventory_system.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.graphics.Color;
import com.mygdx.inventory_test.InventoryTest;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.useGL30 = true;
		config.title = "GDX Inventory System";
		config.resizable = true;
//		config.width = 1920;
//		config.height = 1080;
		config.initialBackgroundColor = Color.GREEN;
		config.fullscreen = false;
		new LwjglApplication(new InventoryTest(), config);
	}
}
